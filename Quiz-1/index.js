//soal 1
var kalimat_1 = "Halo nama saya Chrisanta Veronica Tarigan"
var kalimat_2 = "Saya Santa"
function WordCount (kalimat_1, _kalimat_2){
    return kalimat_1.split(' ').length;
}
console.log(WordCount(kalimat_1))

//soal 2 Function Penghasil Tanggal Esok
var tanggal = 28
var bulan = 12
var tahun = 1998

function tanggalBesok(tanggal, bulan, tahun){
    var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    var selectedMonths = months[bulan - 1];
    var fusion = selectedMonths + '/' + tanggal.toString() + '/' + tahun.toString();
    var s = new Date(fusion);
    s.setDate(s.getDate() + 1);
    return `${s.getDate()} ${months[s.getMonth()]} ${s.getFullYear()}`;
}

var besok = tanggalBesok(tanggal, bulan, tahun);
console.log(besok); 